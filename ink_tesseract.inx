<?xml version="1.0" encoding="UTF-8"?>

<!--Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]-->

<!--        This program is free software; you can redistribute it and/or modify-->
<!--        it under the terms of the GNU General Public License as published by-->
<!--        the Free Software Foundation; either version 2 of the License, or-->
<!--        (at your option) any later version.-->

<!--        This program is distributed in the hope that it will be useful,-->
<!--        but WITHOUT ANY WARRANTY; without even the implied warranty of-->
<!--        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the-->
<!--        GNU General Public License for more details.-->

<!--        You should have received a copy of the GNU General Public License-->
<!--        along with this program; if not, write to the Free Software-->
<!--        Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.-->


<!--        #############################################################################-->
<!--        Ink Tesseract - a partially completed text recognition extension -->
<!--        # An Inkscape 1.1+ extension -->
<!--        #############################################################################-->

<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">
    <name>Ink Tesseract</name>
    <id>inklinea.ink_tesseract</id>


    <param name="ink_tesseract_notebook" type="notebook">

    <page name="main_page" gui-text="Main">

        <hbox>
            <param name="image_source_combo" type="optiongroup" appearance="combo" gui-description="Selected - One or more selected objects
            &#10;&#10;Page - Everything within the page boundaries
            &#10;&#10;Drawing - Everything in the document (even if outside the page)
            &#10;&#10;File - An external file as selected below" gui-text="Source">
            <option value="selected">Selected</option>
            <option value="page">Page</option>
            <option value="drawing">Drawing</option>
            <option value="file">External File</option>
            </param>

            <param name="sampling_dpi_cb" type="boolean" gui-text="Sampling dpi" gui-description="Set custom sampling dpi - by default it will be 96dpi&#10;However to capture smaller details can&#10;be increased (with a speed penalty)">false</param>
            <param name="sampling_dpi_int" type="int" min="96" max="2000" gui-text="" gui-description="Sampling dpi value">300</param>
            <param name="svg_dpi_cb" type="boolean" gui-text="svg dpi" gui-description="Set custom svg dpi - by default it will be 96dpi&#10;Use this to adjust for svg documents&#10;with other dpi values&#10;e-notepad files for example">false</param>
            <param name="svg_dpi_int" type="int" min="1" max="2000" gui-text="" gui-description="Custom svg dpi value">96</param>

        </hbox>
        <vbox>
            <param type="path" name="external_image_filepath" gui-text="External File" gui-description="Import external bitmap or svg file" mode="file">None Selected</param>
        </vbox>



        <hbox>
            <param name="canvas_output_cb" type="boolean" gui-text="Canvas" gui-description="Create Text Elements on Canvas">true</param>

        </hbox>

        <separator/>
        <hbox>
            <param name="file_output_cb" type="boolean" gui-text="File(s)" gui-description="Save Tesseract Output Files ( with options on settings page )">false</param>
            <param name="raw_text_cb" type="boolean" gui-text="Raw Text" gui-description="Send raw text to a popup message box">true</param>
        </hbox>
        <vbox>
            <param type="path" name="export_folder" gui-text="Export Folder" mode="folder">None Selected</param>
        </vbox>

        <hbox>
            <param name="base_filename_string" type="string" gui-description="Base for exported filenames ( for example - ocr_output.pdf / ocr_output.txt etc" gui-text="Base Filename">ocr_output</param>
            <param name="subfolder_output_cb" type="boolean" gui-text="Subfolders" gui-description="Create a Subfolder for each output run">false</param>
            <param name="subfolder_format_combo" type="optiongroup" appearance="combo" gui-description="Naming of Subfolders" gui-text="Naming">
                <option value="timestamp">Timestamp</option>
                <option value="uuid">Uuid</option>
                <option value="random">Random No</option>
            </param>
        </hbox>
    </page>

        <page name="canvas_page" gui-text="Canvas">
            <hbox><label>Inkscape Canvas Output (hocr)</label></hbox>
            <hbox>
                <label xml:space="preserve">Page         </label>
                <param name="page_text_cb" type="boolean" gui-text="Text" gui-description="Create Page Text Elements on Canvas">true</param>
                <param name="page_text_cp" type="color" appearance="colorbutton" gui-text="">0xff0000ff</param>
                <param name="page_paths_cb" type="boolean" gui-text="Paths" gui-description="Create Page Paths on Canvas">true</param>
                <param name="page_paths_cp" type="color" appearance="colorbutton" gui-text="">0xff0000ff</param>
            </hbox>
            <hbox>
                <label xml:space="preserve">Content Area </label>
                <param name="content_area_text_cb" type="boolean" gui-text="Text" gui-description="Create Content Area Text Elements on Canvas">true</param>
                <param name="content_area_text_cp" type="color" appearance="colorbutton" gui-text="">0x00ff00ff</param>
                <param name="content_area_paths_cb" type="boolean" gui-text="Paths" gui-description="Create Content Area Paths on Canvas">true</param>
                <param name="content_area_paths_cp" type="color" appearance="colorbutton" gui-text="">0x00ff00ff</param>
            </hbox>
            <hbox>
                <label xml:space="preserve">Paragraph    </label>
            <param name="paragraph_text_cb" type="boolean" gui-text="Text" gui-description="Create Paragraph Text Elements on Canvas">true</param>
                <param name="paragraph_text_cp" type="color" appearance="colorbutton" gui-text="">0x0000ffff</param>
                <param name="paragraph_paths_cb" type="boolean" gui-text="Paths" gui-description="Create Paragraph Paths on Canvas">true</param>
                <param name="paragraph_paths_cp" type="color" appearance="colorbutton" gui-text="">0x0000ffff</param>
            </hbox>
            <hbox>
                <label xml:space="preserve">Line         </label>
            <param name="line_text_cb" type="boolean" gui-text="Text" gui-description="Create Line Text Elements on Canvas">true</param>
                <param name="line_text_cp" type="color" appearance="colorbutton" gui-text="">0xff00ffff</param>
                <param name="line_paths_cb" type="boolean" gui-text="Paths" gui-description="Create Line Paths on Canvas">true</param>
                <param name="line_paths_cp" type="color" appearance="colorbutton" gui-text="">0xff00ffff</param>
            </hbox>
            <hbox>
                <label xml:space="preserve">Word         </label>
                <param name="word_text_cb" type="boolean" gui-text="Text" gui-description="Create Individual Word Elements on Canvas">true</param>
                <param name="word_text_cp" type="color" appearance="colorbutton" gui-text="">0x00ffffff</param>
                <param name="word_paths_cb" type="boolean" gui-text="Paths" gui-description="Create Individual Word Paths on Canvas">true</param>
                <param name="word_paths_cp" type="color" appearance="colorbutton" gui-text="">0x00ffffff</param>
            </hbox>
            <separator/>
            <hbox>
                <vbox>



                </vbox>
                <vbox>


                </vbox>

            </hbox>

        </page>

    <page name="settings_page" gui-text="Settings">

        <hbox>
             <param name="psm_combo" type="optiongroup" appearance="combo" gui-text="Layout analysis --psm">
                 <option value="3">3 = Fully automatic page segmentation&#10;but no OSD. (Default)</option>
                 <option value="0">0 = Orientation and script detection (OSD) only.</option>
                 <option value="1">1 = Automatic page segmentation with OSD.</option>
                 <option value="2">2 = Automatic page segmentation&#10;but no OSD, or OCR. (not implemented)</option>
                 <option value="4">4 = Assume a single column of text of variable sizes.</option>
                 <option value="5">5 = Assume a single uniform block&#10;of vertically aligned text.</option>
                 <option value="6">6 = Assume a single uniform block of text.</option>
                 <option value="7">7 = Treat the image as a single text line.</option>
                 <option value="8">8 = Treat the image as a single word.</option>
                 <option value="9">9 = Treat the image as a single word in a circle.</option>
                 <option value="10">10 = Treat the image as a single character.</option>
                 <option value="11">11 = Sparse text. Find as much text&#10;as possible in no particular order.</option>
                 <option value="12">12 = Sparse text with OSD.</option>
                 <option value="13">13 = Raw line. Treat the image as a single text line</option>
             </param>
        </hbox>
         <hbox>

             <param name="oem_combo" type="optiongroup" appearance="combo" gui-text="OCR Engine mode --oem">
                 <option value="3">3 = Default, based on what is available.</option>
                 <option value="0">0 = Original Tesseract only.</option>
                 <option value="1">1 = Neural nets LSTM only.</option>
                 <option value="2">2 = Tesseract + LSTM.</option>
             </param>

         </hbox>

    <separator/>
    <hbox>
        <param name="custom_dpi_cb" type="boolean" gui-text="Custom dpi" gui-description="Specify the resolution N in DPI for the input image(s). A typical
           value for N is 300. Without this option, the resolution is read
           from the metadata included in the image. If an image does not
           include that information, Tesseract tries to guess it.">false</param>
        <param name="dpi_int" type="int" min="10" max="10000" gui-text="" gui-description="Set Custom dpi">300</param>
    </hbox>
    <separator/>

    <vbox>

        <label>Export Configurations</label>
        <hbox>
            <param name="export_alto_cb" type="boolean" gui-text="Alto" gui-description="Export to Tesseract Alto XML format">true</param>
            <param name="export_hocr_cb" type="boolean" gui-text="Hocr" gui-description="Export to hocr format">true</param>
            <param name="export_pdf_cb" type="boolean" gui-text="PDF" gui-description="Export to Portable Document Format (pdf)">true</param>
            <param name="export_tsv_cb" type="boolean" gui-text="TSV" gui-description="Export to tsv format">true</param>
        </hbox>
        <hbox>
            <param name="export_txt_cb" type="boolean" gui-text="TXT" gui-description="Export plain text format">true</param>
            <param name="export_get_images_cb" type="boolean" gui-text="Images" gui-description="Write processed input images to file
               (tessinput.tif)">true</param>
            <param name="export_logfile_cb" type="boolean" gui-text="Logfile" gui-description="Redirect debug messages to file (tesseract.log)">true</param>
            <param name="export_lstm_train_cb" type="boolean" gui-text="Training" gui-description="lstm.train — Output files used by LSTM training
               (OUTPUTBASE.lstmf).">true</param>
            <param name="export_makebox_cb" type="boolean" gui-text="Makebox" gui-description="makebox — Write box file (OUTPUTBASE.box)">true</param>
        </hbox>

    </vbox>

    <separator/>

    <vbox>
        <hbox>
            <param name="languages" type="string" gui-description="Specify language(s) used for OCR (LANG[+LANG]).
                    &#10;For languanges other than 'eng' you need to have installed the appropiate .traineddata file
                    &#10;https://github.com/tesseract-ocr/tessdoc/blob/main/Data-Files.md"
                gui-text="Language(s) (separator: +)">eng</param>
        </hbox>
        <label appearance="url">
            https://github.com/tesseract-ocr/tessdoc/blob/main/Data-Files.md
        </label>
    </vbox>

    </page>

        <page name="about_page" gui-text="About">

            <label>
                Ink Tesseract a partially completed Text OCR extension
            </label>
            <label>
                Inkscape 1.1 +
            </label>

            <label appearance="url">

                https://gitlab.com/inklinea

            </label>

            <label xml:space="preserve">

▶ The extension creates layers paragraphs / lines / words
    Just right click a layer and solo the one you want.

▶ Tesseract is included with most Linux distributions
▶ Appears in 'Extensions>Images
▶ Windows Binaries:
		</label>

            <label appearance="url">
                https://github.com/UB-Mannheim/tesseract/wiki
            </label>

        </page>

    </param>
	
    <effect>
        <object-type>all</object-type>
        <effects-menu>
            <submenu name="Ink Tesseract"/>

        </effects-menu>
    </effect>
    <script>
        <command location="inx" interpreter="python">ink_tesseract.py</command>
    </script>
</inkscape-extension>
